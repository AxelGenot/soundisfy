// Appels d'API

import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

export const shazamCoreApi = createApi({
    reducerPath: 'shazamCoreApi',
    baseQuery: fetchBaseQuery({
        baseUrl: 'https://shazam-core7.p.rapidapi.com',
        prepareHeaders: (headers) => {
            headers.set('X-RAPIDAPI-Key', '823df8c624msh552e66ee3b128a5p1add92jsn9b87bde71991');
            return headers;
        }
    }),

    endpoints: (builder) => ({
        getTopCharts: builder.query({ query: () => '/charts/get-top-songs-in-world' })
    })
});

export const {
    useGetTopChartsQuery,
} = shazamCoreApi;